/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dept;

import Annotation.MyAnnotation;
import Annotation.Url_ClassMethod_HashMap;
import ModelView.ModelView;
import java.util.ArrayList;

/**
 *
 * @author User_HP
 */
public class Departement{
    int id;
    String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @MyAnnotation(url = "listerDepartement")
    public ModelView lister() throws Exception{
        ModelView mv = new ModelView();
        
        ArrayList<Departement> listesDepartements = new ArrayList<>();
        Departement dept1 = new  Departement();
        dept1.setId(1);
        dept1.setNom("Economie");
        Departement dept2 = new  Departement();
        dept2.setId(2);
        dept2.setNom("Gestion");
        listesDepartements.add(dept1);
        listesDepartements.add(dept2);
        Url_ClassMethod_HashMap hmap = new Url_ClassMethod_HashMap();
        hmap.setNomRecup("listeDept");
        hmap.setValeurRecup(listesDepartements);
        mv.setUrl("listeDept.jsp");
        mv.setData(hmap);
        
        return mv;
    }
    
    @MyAnnotation(url = "save")
    public ModelView save() throws Exception {
        ModelView mv = new ModelView();
        
        ArrayList<Departement> listesDepartements = new ArrayList<>();
        Departement dept1 = new  Departement();
        dept1.setId(1);
        dept1.setNom("Economie");
        Departement dept2 = new  Departement();
        dept2.setId(2);
        dept2.setNom("Gestion");
        listesDepartements.add(dept1);
        listesDepartements.add(dept2);
        listesDepartements.add(this);
        
        Url_ClassMethod_HashMap hmap = new Url_ClassMethod_HashMap();
        hmap.setNomRecup("valeur");
        hmap.setValeurRecup(listesDepartements);
        mv.setUrl("getValeur.jsp");
        mv.setData(hmap);
        
        return mv;
    }
}
