<%-- 
    Document   : getValeur
    Created on : 9 nov. 2022, 22:33:51
    Author     : User_HP
--%>
<%@page import="Dept.Departement"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Departement> listesValeurs = (ArrayList)request.getAttribute("valeur");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Get valeur</title>
    </head>
    <body>
    <center>
        <table border="1">
            <h2>Listes Départements</h2>
            <tr>
                <th>Id</th>
                <th>Nom</th>
            </tr>
            <% for (int i = 0; i < listesValeurs.size(); i++) { %>
            <tr>
                <td><% out.print(listesValeurs.get(i).getId()); %></td>
                <td><% out.print(listesValeurs.get(i).getNom()); %></td>
            </tr>
            <% }%>
        </table>
        <p><a href="<%=request.getContextPath()%>\index.jsp"><button>Retour</button></a></p>
    </center>
    </body>
</html>
