<%-- 
    Document   : listeDept
    Created on : 21 oct. 2022, 16:48:06
    Author     : User_HP
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="Dept.Departement"%>
<%
    ArrayList<Departement> listeDept = (ArrayList) request.getAttribute("listeDept");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Liste</title>
    </head>
    <body>
    <center>
        <table border="1">
            <h2>Listes Départements</h2>
            <tr>
                <th>Id</th>
                <th>Nom</th>
            </tr>
            <% for (int i = 0; i < listeDept.size(); i++) { %>
            <tr>
                <td><% out.print(listeDept.get(i).getId()); %></td>
                <td><% out.print(listeDept.get(i).getNom()); %></td>
            </tr>
            <% }%>
        </table>
        <p><a href="<%=request.getContextPath()%>\index.jsp"><button>Retour</button></a></p>
    </center>
</body>
</html>
